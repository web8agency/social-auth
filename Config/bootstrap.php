<?php

if(!defined('FACEBOOK_APP_ID')) trigger_error('FACEBOOK_APP_ID is not defined');
if(!defined('FACEBOOK_APP_SECRET')) trigger_error('FACEBOOK_APP_SECRET is not defined');

if(!defined('GOOGLE_APP_ID')) trigger_error('GOOGLE_APP_ID is not defined');
if(!defined('GOOGLE_APP_SECRET')) trigger_error('GOOGLE_APP_SECRET is not defined');


Configure::write('FacebookUseJavascript', false);
Configure::write('FacebookPermissions', ['email']);
Configure::write('FacebookUrlRedirect', 'social/login/facebook');
Configure::write('GoogleUrlRedirect', 'social/login/google');

CakeLog::config('socialAuth', array(
    'engine' => 'FileLog',
    'types' => array('socialAuth'),
    'file' => 'socialAuth',
));