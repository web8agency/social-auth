<?php
class SocialId extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 */
	public $description = 'social_id';

/**
 * Actions to be performed
 *
 * @var array $migration
 */
	public $migration = array(
		'up' => array(
			'create_field' => array(
				'users' => array(
					'social_id' => array('type' => 'biginteger', 'null' => true, 'default' => null, 'unsigned' => false),
				),
			),
		),
		'down' => array(
			'drop_field' => array(
				'users' => array('social_id'),
			),
		),
	);

/**
 * Before migration callback
 *
 * @param string $direction Direction of migration process (up or down)
 * @return bool Should process continue
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction Direction of migration process (up or down)
 * @return bool Should process continue
 */
	public function after($direction) {
		return true;
	}
}
