jQuery(function($){
    $('a.btn-facebook').click(function(e){
        e.preventDefault();
        var url = $(this).attr('href');
        if(!url) url = '/social/login/facebook';

        FB.login(function(response){
            if(response.authResponse){
                window.location = url;
            }
        },{scope : 'public_profile,email'});
        return false;
    });

    $('.facebookDeconnect').click(function(e){
        e.preventDefault();
        var url = $(this).attr('href');
        FB.logout(function(response){
            if(response.authResponse){
                window.location = url;
            }
        });
    });

    $('.btn-facebook.btn-share').on('click', function (e) {
      e.preventDefault();
      var url = $(this).attr('href');
      FB.ui({
          method: 'share',
          href: url
      }, function(response){});
    });
});


window.fbAsyncInit = function() {
  FB.init({
      appId      : '219830255080865',
      cookie     : true,  // enable cookies to allow the server to access
                          // the session
      xfbml      : true,  // parse social plugins on this page
      version    : 'v2.7' // use version 2.7
  });
};

(function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/fr_FR/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));