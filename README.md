# SocialAuth v1.1.6

SocialAuth is a plugin for CakePHP to use Facebook and Google Login

* [CakePHP](http://book.cakephp.org/2.0/fr/index.html)
* [Facebook SDK](https://github.com/facebook/php-graph-sdk)
* [Google API Client](https://github.com/google/google-api-php-client)

## Requirements

* CakePHP >= 2.7.0
* Facebook SDK
* Google API Client

## Installation

Ensure require is present in composer.json. This will install the plugin into Plugin/CakeHelper:

	{
		"require": {
			"web8agency/social-auth": "*"
		}
	}

### Enable plugin

You need to enable the plugin in your app/Config/bootstrap.php file:

`CakePlugin::load('SocialAuth');`

If you are already using `CakePlugin::loadAll();`, then this is not necessary.


### Enable Facebook Javascript

If you want to enbable Facebook Javascript SDK, add to your bootstrap.php

	`Configure::write('FacebookUseJavascript', true);`