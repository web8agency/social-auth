<?php
App::uses('BaseAuthenticate', 'Controller/Component/Auth');

class SocialAuthenticate extends BaseAuthenticate {
    public function authenticate(CakeRequest $request, CakeResponse $response) {
        return $this->getUser($request);
    }


    public function getUser(CakeRequest $request) {
        $user = false;

        if($request->url == Configure::read('FacebookUrlRedirect')){
            $user = $this->_getFacebookUser();
        }

        if($request->url == Configure::read('GoogleUrlRedirect')){
            $user = $this->_getGoogleUser($request);
        }

        if(!$user) return false;

        return $this->_findUser($user);
    }

    protected function _findUser($username, $password = null)
    {
        $userModel = $this->settings['userModel'];
        list(, $model) = pluginSplit($userModel);

        $userModel = ClassRegistry::init($userModel);
        $result = $userModel->find('first', array(
            'conditions' => [
                $model . '.social_id' => $username['id']
            ],
            'recursive' => $this->settings['recursive'],
            'contain' => $this->settings['contain'],
        ));

        if (empty($result[$model])) {
            $result = $userModel->find('first', array(
                'conditions' => [
                    $model . '.mail' => $username['email']
                ],
                'recursive' => $this->settings['recursive'],
                'contain' => $this->settings['contain'],
            ));

            if (empty($result[$model])) {  // creation nouveau compte
                $userModel->create([
                    $model => [
                        'mail' => $username['email'],
                        'social_id' => $username['id'],
                        'active' => 0,
                        'role_id' => 5 // client
                    ],
                    'Profile' => [
                        'civility' => ($username['gender'] == 'male')?'M.':'Mme',
                        'lastname' => $username['last_name'],
                        'firstname' => $username['first_name'],
                    ]
                ]);

                $userModel->validator()->remove('password');
                if(!$userModel->saveAssociated()) return false;
                $userModel->activate($userModel->id);
            }
            else {                      // fusion compte existant
                $userModel->id = $result[$model]['id'];
                if(!$userModel->saveField('social_id', $username['id'])) return false;
            }

            $result = $userModel->find('first', array(
                'conditions' => [
                    $model . '.social_id' => $username['id']
                ],
                'recursive' => $this->settings['recursive'],
                'contain' => $this->settings['contain'],
            ));
        }
        else if($result[$model]['mail'] != $username['email']) return false;

        $user = $result[$model];

        unset($result[$model]);
        return array_merge($user, $result);
    }

    protected function _getFacebookUser(){
        $fb = new Facebook\Facebook([
            'app_id' => FACEBOOK_APP_ID,
            'app_secret' => FACEBOOK_APP_SECRET,
            'default_graph_version' => 'v2.7'
        ]);

        if(Configure::read('FacebookUseJavascript'))
            $helper = $fb->getJavaScriptHelper();
        else
            $helper = $fb->getRedirectLoginHelper();

        try {
            $accessToken = $helper->getAccessToken();
            $response = $fb->get('/me?fields=id,name,email,first_name,last_name,gender,picture', $accessToken);
            $user = $response->getGraphUser();
            return $user->all();
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            CakeLog::write('socialAuth', 'Facebook Graph returned an error: ' . $e->getMessage());
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            CakeLog::write('socialAuth', 'Facebook SDK returned an error: ' . $e->getMessage());
        } catch(\Exception $e) {
            CakeLog::write('socialAuth', $e->getMessage());
        }

        return false;
    }


    protected function _getGoogleUser($request){
        $client = new Google_Client();
        $client->setClientId(GOOGLE_APP_ID);
        $client->setClientSecret(GOOGLE_APP_SECRET);
        $client->setRedirectUri(FULL_BASE_URL.'/'.Configure::read('GoogleUrlRedirect'));

        $client->setScopes(array(
            "https://www.googleapis.com/auth/userinfo.profile",
            'https://www.googleapis.com/auth/userinfo.email'
        ));
        $client->setApprovalPrompt('auto');

        /* si dans l'url le paramètre de retour Google contient 'code' */
        if (isset($request->query['code'])) {
            // Alors nous authentifions le client Google avec le code reçu
            $client->authenticate($request->query['code']);
            // et nous plaçons le jeton généré en session
            CakeSession::write('google_token', $client->getAccessToken());
        }

        /* si un jeton est en session, alors nous le plaçons dans notre client Google */
        if (CakeSession::check('google_token')) {
            $client->setAccessToken(CakeSession::read('google_token'));
        }

        /* Si le client Google a bien un jeton d'accès valide */
        if ($client->getAccessToken()) {
            // alors nous écrivons le jeton d'accès valide en session
            CakeSession::write('google_token', $client->getAccessToken());
            // nous créons une requête OAuth2 avec le client Google paramétré
            $oauth2 = new Google_Service_Oauth2($client);
            // et nous récupérons les informations de l'utilisateur connecté
            try{
                $user = $oauth2->userinfo->get();

                return [
                    'id' => $user->getId(),
                    'email' => $user->getEmail(),
                    'first_name' => $user->getGivenName(),
                    'last_name' => $user->getFamilyName(),
                    'gender' => $user->getGender(),
                ];
            } catch(Google_Exception $e){
                CakeLog::write('socialAuth', 'Google: ' . $e->getMessage());
            } catch(\Exception $e){
                CakeLog::write('socialAuth', 'Google: ' . $e->getMessage());
            }
        }

        return false;
    }
}