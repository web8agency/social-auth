<?php
App::uses('AppController', 'Controller');
/**
 * Social Controller
 *
 */
class SocialController extends AppController{
    public $uses = [];

    public function beforeFilter()
    {
        parent::beforeFilter();

        $this->Auth->allow('login');
    }


    public function login($type)
    {
        if($type != 'facebook' && $type != 'google') return $this->redirect('/');

        if ($this->Auth->login()) {
            $this->Flash->success(__d('socialauth', "Vous êtes maintenant connecté."), ['key' => 'auth']);
            return $this->redirect($this->Auth->redirectUrl());
        }
        else {
            $this->Flash->error(__d('socialauth', "Impossible de vous connecter."), ['key' => 'auth']);
        }
        return $this->redirect($this->Auth->loginAction);
    }
}