<?php
App::uses('AppHelper', 'View/Helper');

class SocialHelper extends AppHelper{
    public $helpers = ['Html'];

    public function getUrl($type)
    {
        if($type == 'facebook') return $this->getFacebookLoginUrl();
        if($type == 'google') return $this->getGoogleLoginUrl();

        return '/';
    }

    protected function getFacebookLoginUrl()
    {
        if(Configure::read('FacebookUseJavascript')) $this->Html->script('SocialAuth.facebook', ['inline' => false]);

        $fb = new Facebook\Facebook([
            'app_id' => FACEBOOK_APP_ID,
            'app_secret' => FACEBOOK_APP_SECRET,
            'default_graph_version' => 'v2.7'
        ]);

        $helper = $fb->getRedirectLoginHelper();
        return $helper->getLoginUrl(FULL_BASE_URL.'/'.Configure::read('FacebookUrlRedirect'), Configure::read('FacebookPermissions'));
    }

    protected function getGoogleLoginUrl()
    {
        $client = new Google_Client();
        $client->setClientId(GOOGLE_APP_ID);
        $client->setClientSecret(GOOGLE_APP_SECRET);
        $client->setRedirectUri(FULL_BASE_URL.'/'.Configure::read('GoogleUrlRedirect'));

        $client->setScopes(array(
            "https://www.googleapis.com/auth/userinfo.profile",
            'https://www.googleapis.com/auth/userinfo.email'
        ));
        $url = $client->createAuthUrl();
        return $url;
    }

}